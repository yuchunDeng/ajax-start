var verseChoose = document.querySelector('select');
var poemDisplay = document.querySelector('pre');

verseChoose.onchange = function() {
  var verse = verseChoose.value;
  updateDisplay(verse);
}

verseChoose.value = 'Verse 1';
updateDisplay('Verse 1');

function updateDisplay(verse) 
{

verse = verse.replace(" ", "");
verse = verse.toLowerCase();
var url = 'http://localhost:8000/Desktop/Ajax-start/' + verse + '.txt';



var request = new XMLHttpRequest();
request.open('GET', url);

request.responseType = 'text';



request.onload = function() 
{
  console.log("url=" + url);
  poemDisplay.textContent = request.response;
};

 request.send();

}
//python -m http.server
